# Projekt bazy danych - Wojciech Olech, Łukasz Marzęda 

## 1. Treść zadania

Muzeum historii naturlanej posiada głowna siedzibę oraz kilka filii w różnych krajach. Zarówn o w głonej sidzibie jak i w filiach są wystawy zarówno stałe jak i czasowe ( w rózych salach ozaczonych nunmerami oraz z okresloną lokalizacją). Poza tym muzeum organizuje także okazjonalne czasowe wystawy wyjazdowe.
W muzeum znajduja się eksponaty pogrupowane w kategorie i podkategorie. Każdy eksponat ma okreslone informacje: nazwę, opis, ewntualnie date powstania, autora, a także materiał(materiały)  z którego został wykonany. Eksponaty są czasem przenoszone: do różnych sal, na różne wystawy. Moga być też przesuwane pomiędzy filiami a także sprzedawane. Należy uwzględnić też zakup nowych. W bazie danych muzeum znaleźć powinny informacje nie tylko o miejscach, w których eksponaty są aktualnie przechowywane, ale także o historii ich przesunięć wraz z czasem i okresem przebywania w poszczególnych miejscach. 
Ponadto eksponaty są okresowo poddawane konserwacji. Konieczna jest ewidencja tych prac wraz z informacją o niezbędnych materiałach konserwacyjnych oraz zamównieniach tych materiałów od dostawców. 
Uwzględnione muszą być także koszty: zatrudnienia pracowników, koszty biletów (różnego rodzaju wraz z uwzględnieniem wystaw czasowych), koszty zakupu i sprzedaży ekspoantów oraz koszty organizacji wystaw czasowych oraz wyjazdowych. 

## 2. Opis tabel

| Nazwa tabeli                 | Typ      | Opis                                       |
|------------------------------|----------|--------------------------------------------|
| Siedziby                     | Dane     | Informacje o siedzibach                    |
| Wystawy                      | Dane     | Informacje o wystawach                     |
| Wystawy wyjazdowe            | Dane     | Informacje o wystawach wyjazdowych         |
| Kategorie eksponatow         | Dane     | Informacje o kategoriach eksponatów        |
| Eksponaty                    | Dane     | Informacje o eksponatach                   |
| Sale                         | Dane     | Informacje o salach                        |
| Tabela lokalnych przesuniec eksponatow | Podzbiór | Informacje o przesunięciach między filiami |
| Tabela zewnętrznych przesunięć eksponatow | Podzbiór | Informacje o przesunięciach między zewnętrznymi wystawami |
| Zakup ekponatow              | Podzbiór | Informacje o zakupie eksponatów            |
| Sprzedaż eksponatow          | Podzbiór | Informacje o sprzedaży eksponatów          |
| Konserwacja eksponatow       | Podzbiór | Informacje o konserwacji                   |
| Konserwacja - materialy      | Łącząca  | Informacje o materiałach wykorzystywanych podczas konserwacji |
| Materialy konserwacyjne      | Dane     | Informacje o materiałach konserwacyjnych   |
| Zamowienia materialow        | Podzbiór | Informacje o zamówionych materiałach       |
| Dostawcy                     | Dane     | Informacje o dostawcach materiałów         |
| Pracownicy                   | Dane     | Informacje o pracownikach                  |
| Bilety                       | Podzbiór | Informacji o biletach                      | 
| Bilety - wystawy lokalne     | Łącząca  | Informacje o biletach przypisanych do wystaw lokalnych | 
| Bilety - wystawy zewnetrzne  | Łącząca  | Informacje o biletach przypisanych do wystaw zewnętrznych | 
 
## 3. Pola tabel

### Siedziby

| Nazwa pola           | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                                       |
|----------------------|-------------|---------------|--------------------|--------------------------------------------|
| id_Siedziby **(id)** | INT         | TAK           | TAK                | Identyfikator siedziby muzeum, głowny klucz |
| nazwa                | VARCHAR(60) | TAK           | TAK                | Ciąg znaków okreslający nazwę muzeum       |
| miejscowosc          | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslający miejscowość        |
| ulica                | VARCHAR(60) | TAK           | NIE                | Ciag znaków określajacy ulicę              |
| nrLokum              | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslający nr mieszkania      |
| kodPocztowy          | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków określający kod pocztowy       |
| miastoPoczty         | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslający miasto             |
| telefonKontaktowy    | VARCHAR(11) | TAK           | TAK                | Ciąg znaków okreslający nr telefonu        |
| czyGlowna            | TINYINT     | TAK           | NIE                | Zmienna określająca czy siedziba jest siedzbą głowną  |

### Wystawy

| Nazwa pola            | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                 |
|-----------------------|--------------|---------------|--------------------|--------------------------------------|
| id_Wystawy  **(id)**  | INT          | TAK           | TAK                | Identyfikator wystawy, główny klucz  |
| nazwa                 | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslajacy nazwę        |
| dataStart             | TIMEDATE     | TAK           | NIE                | Data w której wystawa się zaczęła    |
| dataKoniec            | TIMEDATE     | NIE           | NIE                | Data w której wystawa się zakończyła |
| opis                  | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis wystawy  |
| koszty                | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca poniesione koszty | 
| id_Sali  **(obcy)**   | INT          | TAK           | NIE                | Identyfikator sali w siedzibie        |

### Wystawy wyjazdowe

| Nazwa pola           | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                      |
|----------------------|--------------|---------------|--------------------|-------------------------------------------|
| id_Wystawy  **(id)** | INT          | TAK           | TAK                | Identyfikator wystawy, główny klucz       |
| nazwa                | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslajacy nazwę             |
| dataStart            | TIMEDATE     | TAK           | NIE                | Data w której wystawa się zaczęła         |
| dataKoniec           | TIMEDATE     | NIE           | NIE                | Data w której wystawa się zakończyła      |
| opis                 | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis wystawy      |
| koszty               | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca poniesione koszty |
| miejscowosc          | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslający miejscowość       |
| ulica                | VARCHAR(60)  | TAK           | NIE                | Ciag znaków określajacy ulicę             |
| nrLokum              | VARCHAR(5)   | TAK           | NIE                | Ciąg znaków okreslający nr mieszkania     |
| kodPocztowy          | VARCHAR(6)   | TAK           | NIE                | Ciąg znaków określający kod pocztowy      |
| miastoPoczty         | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslający miasto            |

### Kategorie eksponatów

| Nazwa pola                        | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------------|-------------|---------------|--------------------|----------------------------|
| id_Kategorii   **(id)**           | INT         | TAK           | TAK                | Identyfikator kategorii ekponatu, głowny klucz |
| nazwa                             | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy okreslający nazwę        |
| id_KategoriiNadrzednej **(obcy)** | INT         | NIE           | NIE                | Identyfikator kategorii nadrzędnej    |

### Eksponaty

| Nazwa pola              | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                     |
|-------------------------|--------------|---------------|--------------------|------------------------------------------|
| id_Eksponatu  **(id)**  | INT          | TAK           | TAK                | Identyfikator eksponatu, głowny klucz    |
| nazwa                   | VARCHAR(60)  | TAK           | NIE                | Ciąg znakowy okreslający nazwę eksponatu |
| opis                    | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis eksponatu   |
| dataPowstania           | DATETIME     | NIE           | NIE                | Data powstania eksponatu                 |
| autor                   | VARCHAR(60)  | NIE           | NIE                | Ciąg znakowy określający autora          |
| material                | VARCHAR(60)  | NIE           | NIE                | Ciąg znakowy okreslający materiał        |
| id_Kategorii **(obcy)** | INT          | NIE           | NIE                | Identyfikator kategori do której należy eksponat |

### Sale

| Nazwa pola              | Typ        | Czy wymagane? | Wartość unikatowa? | Opis                           |
|-------------------------|------------|---------------|--------------------|--------------------------------|
| id_Sali     **(id)**    | INT        | TAK           | TAK                | Identyfikator sali, głowny klucz   |
| nazwa                   | VARCHAR(60)| TAK           | NIE                | Ciąg znaków okreslający nazwę sali | 
| id_Siedziby **(obcy)**  | INT        | TAK           | NIE                | Identyfikator siedziby w której jest sala |


### Tabela lokalnych przesunięć eksponatów

| Nazwa pola               | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                                        |
|--------------------------|----------|---------------|--------------------|---------------------------------------------|
| id_Przesuniecia **(id)** | INT      | TAK           | TAK                | Identyfikator przesunięcia eksponatu, głowny klucz     |
| dataPrzesuniecia         | TIMEDATE | TAK           | NIE                | Data przesuniecia eksponatu                            |
| id_Eksponatu **(obcy)**  | INT      | TAK           | NIE                | Identyfikator przenoszonego eksponatu                  |
| id_saliZ **(obcy)**      | INT      | NIE           | NIE                | Identyfikator sali z której eksponat jest przenoszony  |
| id_saliDo **(obcy)**     | INT      | NIE           | NIE                | Identyfikator sali do której eksponat jest przenoszony |

### Tabela zewnętrzych przesunięc eksponatów

| Nazwa pola               | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                                           |
|--------------------------|----------|---------------|--------------------|------------------------------------------------|
| id_Przesuniecia **(id)** | INT      | TAK           | TAK                | Identyfikator przesunięcia eksponatu, główny klucz        |
| dataPrzesuniecia         | TIMEDATE | TAK           | NIE                | Data przesuniecia eksponatu                               |
| id_Eksponatu **(obcy)**  | INT      | TAK           | NIE                | Identyfikator przenoszonego eksponatu                     |
| id_wystawyZ **(obcy)**   | INT      | NIE           | NIE                | Identyfikator wystawy z której eksponat jest przenoszony  |
| id_wystawyDo **(obcy)**  | INT      | NIE           | NIE                | Identyfikator wystawy do której eksponat jest przenoszony |


### Zakup eksponatów

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Zakupu **(id)**          | INT         | TAK           | TAK                | Identyfikator zakupu eksponatu, głowny klucz| 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmienno przecinkowa okreslająca cenę zakupu netto |
| data                        | DATETIME    | TAK           | NIE                | Data zakupu |
| imieSprzedajacego           | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający imię        |
| nazwiskoSprzedajacego       | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający nazwisko    |
| nazwaFirmy                  | VARCHAR(60) | NIE           | NIE                | Ciąg znaków okreslajacy nazwę firmy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miejscowość |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy ulicę       |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslajacy nr mieszkania |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków okreslajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miasto Pocztowe |
| id_SaliDocelowej **(obcy)** | INT         | NIE           | NIE                | Identyfikator sali do której trafia zakupiony eksponat |
| id_Eksponatu **(obcy)**     | INT         | TAK           | TAK                | Identyfikator zakupionego eksponatu |

### Sprzedaż eksponatów 

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Sprzedazy **(id)**       | INT         | TAK           | TAK                | Identyfikator sprzedazy eksponatu,głowny klucz | 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmienno przecinkowa okreslająca cena sprzedazy netto       | 
| data                        | DATETIME    | TAK           | NIE                | Data sprzedaży             |
| imieKupujacego              | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający imię        |
| nazwiskoKupującego          | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający nazwisko    |
| nazwaFirmy                  | VARCHAR(60) | NIE           | NIE                | Ciąg znaków okreslajacy nazwę firmy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miejscowość |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy ulicę       |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslajacy nr mieszkania |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków okreslajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miasto Pocztowe |
| id_Eksponatu **(obcy)**     | INT         | TAK           | TAK                | Identyfikator sprzedanego eksponatu |

### Konserwacje

| Nazwa pola                  | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|----------|---------------|--------------------|------------------------------|
| id_Konserwacji **(id)**     | INT      | TAK           | TAK                | Identyfikator konserwacji, klucz główny |
| dataRozpoczecia             | TIMEDATE | TAK           | NIE                | Data rozpoczęcia konserwacji |
| dataZakonczenia             | TIMEDATE | NIE           | NIE                | Data zakończenia konserwacji |
| id_Eksponatu **(obcy)**     | INT      | TAK           | NIE                | Identyfikator konserwowanego przedmiotu |

#### Konserwacje - materiały

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|--------------|---------------|--------------------|------------------------------|
| id **(id)**                 | INT          | TAK           | TAK                | Identyfikator rekordu                   |
| iloscMaterialu              | DOUBLE       | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca ilość posiadanego materiału      |
| jednostkaMaterialu          | VARCHAR(45)  | TAK           | NIE                | Ciąg znakowy określajacy jednostkę materiału |
| id_Konserwacji **(obcy)**   | INT          | TAK           | NIE                | Identyfikator konserwacji               |
| id_Materialu **(obcy)**     | INT          | TAK           | NIE                | Identyfikator uzytego materialu         |

### Materialy konserwacyjne

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|--------------|---------------|--------------------|------------------------------|
| id_Materialu **(id)**       | INT          | TAK           | TAK                | Identyfikator materiału, klucz głowny   |
| nazwa                       | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków określający nazwę materiału |
| opisMaterialu               | VARCHAR(255) | TAK           | NIE                | Ciąg znaków okreslajacy opis materiału  |
| iloscMaterialu              | DOUBLE       | TAK           | NIE                | Liczba zmiennoprzecinkowa okrelająca ilośc materiału |
| jednostkaMaterialu          | VARCHAR(60)  | TAK           | NIE                | Ciąg znakowy określajacy jednostkę materiału |

### Zamówienia materiałów
  
| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Zakupu **(id)**          | INT         | TAK           | TAK                | Identyfikator zakupu materiału, klucz głowny       | 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca cenę netto   |
| data                        | DATETIME    | TAK           | NIE                | Data zakupu |
| ilosc                       | DOUBLE      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca ilosc zakupionego materialu |
| jednostka                   | VARCHAR(60) | TAK           | NIE                | Jednostka zakupionego materiału                    |
| nazwiskoSprzedajacego       | VARCHAR(45) | TAK           | NIE                | Ciąg znakowy określajacy nazwisko                  |
| imieSprzedajacego           | VARCHAR(45) | TAK           | NIE                | Ciąg znakowy określajacy imie                      |
| firmaSprzedajacego          | VARCHAR(60) | NIE           | NIE                | Ciąg znakowy określajacy nazwa firmy               |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość               |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulica                     |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu              |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy              |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej |

### Dostawcy

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Dostawcy **(id)**        | INT         | TAK           | TAK                | Identyfikator dostawcy, klucz głowny    |
| nazwaDostawcy               | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy nazwa dostawcy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość    |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulicę          |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu   |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy   |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej  |

### Pracownicy 

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Pracownika **(id)**      | INT         | TAK           | TAK                | Identyfikator pracownika, klucz głowny | 
| imie                        | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy imię          |
| nazwisko                    | VARCHAR     | TAK           | NIE                | Ciąg znakowy określajacy nazwisko      |
| pesel                       | VARCHAR(11) | TAK           | NIE                | Ciąg znakowy określajacy pesel         |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość   |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulicę         |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu  |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej | 
| numerTelefonu               | VARCHAR(11) | TAK           | NIE                | Ciąg znakowy określajacy numer telefonu |

### Bilety

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|--------------|---------------|--------------------|----------------------------|
| id_Biletu **(id)**          | INT          | TAK           | TAK                | Identyfikator biletu, klucz główny                |
| cena                        | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca cenę biletu |
| opis                        | VARCHAR(255) | TAK           | NIE                | Ciąg znakowy określajacy opis biletu              |

#### Bilety - wystawy lokalne

| Nazwa pola                   | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                       |
|------------------------------|----------|---------------|--------------------|----------------------------|
| id_BiletWystawa **(id)**     | INT      | TAK           | TAK                | Identyfikator połączenia bilet-wystawa, klucz główny |
| id_Biletu **(obcy)**         | INT      | TAK           | NIE                | Identyfikator biletu                   |
| id_WystawaLokalna **(obcy)** | INT      | TAK           | NIE                | Identyfikator wystawy lokalnej         |

#### Bilety - wystawy zewnętrzne

| Nazwa pola                      | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                       |
|---------------------------------|----------|---------------|--------------------|----------------------------|
| id_BiletWystawa **(id)**        | INT      | TAK           | TAK                | Identyfikator połączenia bilet-wystawa, klucz główny |
| id_Biletu **(obcy)**            | INT      | TAK           | NIE                | Identyfikator biletu                   |
| id_WystawaZewnetrzna **(obcy)** | INT      | TAK           | NIE                | Identyfikator wystawy zewnętrznej      |

## 4. Klucze główne

| Nazwa tabeli            | Nazwa klucza głównego |
|-------------------------|-----------------------|
| Siedziby                | id_Siedziby           |
| Wystawy                 | id_Wystawy            | 
| Wystawy wyjazdowe       | id_Wystawy       |
| Kategoria eksponatów    | id_Kategorii  |
| Eksponaty               | id_Eksponatu     |
| Sale                    | id_Sale              |
| Tabele lokalnych przesunięć eksponatów     | id_Przesuniecia  |
| Tabela zewnętrzych przesunięć eksponatów   | id_Przesuniecla  |
| Zakup eksponatów        | id_Zakupu        |
| Sprzedaż eksponatów     | id_Sprzedazy    |
| Konserwacje             | id_Konserwacji   |
| Konserwacje - Materiały | id         |
| Materiały               | id_Materialu     |
| Zakup materialu         | id_Zakupu        |
| Dostawcy                | id_Dostawcy      | 
| Pracownicy              | id_Pracownika    |
| Bilety                  | id_Biletu        |
| Bilety - wystawy lokalne| id_BiletWystawa  |
| Bilety - wystawy zewnętrzne | id_BiletWystawa | 

## 5. Klucze obce

### Tabela Wystawy 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Sale                    | id_Sali             |

### Tabela Kategorie Eksponatów 
| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Kategorie eksponatów    | id_KategoriiNadgrzednej|

### Tabela Eksponaty 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Kategorie Eksponatów    | id_Kategorii        |

### Tabela Sale

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Siedziby                | id_Siedziby         |

### Tabela lokalnych przesunięć eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |
| Sala                    | id_saliZ            |
| Sala                    | id_saliDo           |

### Tabela zewnętrznych przesunięć eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |
| Wystawa zewnętrzna      | id_wystawyZ         |
| Wystawa zewnętrzna      | id_wystawyDo        |

### Tabela Zakup Eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Sale                    | id_SaliDocelowej    |
| Eksponaty               | id_Eksponaty        |

### Tabela Sprzedaż Ekspoantów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |

### Tabela Konserwacje

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |

### Tabela Konserwacja - Materiały 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Konserwacje             | id_Konserwacji      |
| Materiały               | id_Materiału        |

### Tabela bilety - wystawy lokalne

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Bilety                  | id_Biletu           |
| Wystawy                 | id_WystawaLokalna   |

### Tabela bilety - wystawy zewnętrzne

| Nazwa tabeli pierwotnej | Nazwa klucza obcego  |
|-------------------------|----------------------|
| Bilety                  | id_Biletu            |
| Wystawy zewnętrzne      | id_WystawaZewnetrzna |

## 6. Macierz relacji

<table>
    <thead>
        <tr>
            <th>Tabela/Tabela</th>
            <th>Siedziby</th>
            <th>Wystawy</th>
            <th>Wystawy wyjazdowe</th>
            <th>Kategorie eksponatów</th>
            <th>Eksponaty</th>
            <th>Sale</th>
            <th>Przesunięcia lokalne</th>
            <th>Przesunięcia zewnętrzne</th>
            <th>Zakup eksponatów</th>
            <th>Sprzedaż eksponatów</th>
            <th>Konserwacja eksponatów</th>
            <th>Zamówienia materiałów</th>
            <th>Dostawcy</th>
            <th>Pracownicy</th>
            <th>Bilety</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Siedziby</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Wystawy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td>n:1</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Wystawy wyjazdowe</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Kategorie eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td>n:1</td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Eksponaty</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td>n:1</td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Sale</b></td>
            <td>n:1</td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Przesunięcia lokalne</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td>n:m (?)</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Przesunięcia zewnętrzne</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td>n:m (?)</td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Zakup eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>1:1</td> <!-- Eksponaty --> 
            <td>n:1</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Sprzedaż eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>1:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Konserwacja eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Zamówienia materiałów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        <tr>
            <td><b>Dostawcy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Pracownicy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Bilety</b></td>
            <td></td> <!-- Siedziby -->
            <td>n:m</td> <!-- Wystawy --> 
            <td>n:m</td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
    </tbody>
</table>

## 7. ERD

<img src="erd.svg" style="width:100%"/>

## 8. Reguły usuwania rekordów

| Rodzaj związku                     | Reguła usuwania danych z bazy        |
|------------------------------------|--------------------------------------|
| sale_zakupEksponatow               | Ustawienie wartości 'null' dla atrybutu w tabeli Zakup Eksponatów |
| eksponaty_zakupeksponatów          | usuwanie kaskadowe   |
| eksponaty_kategorieEksponatów      | Ustawienie wartości 'null' dla atrybutu w tabeli Eksponaty |
| eksponaty_konserwacje              | usuwanie kaskadowe                   |
| konserwacje_konserwacje_MateriałyKonserwacyjne | usuwanie kaskadowe |
| konserwacje-MaterialyKonserwacyjne_MaterialyKonseracyjne | usuwanie kaskadowe |
| sprzedaż_Eksponatów_Eksponaty      | usuwanie kaskadowe               |
| eksponaty_tabelaLokalnychPrzesuniec | usuwanie kaskadowe |
| eksponaty_tabelaZewnetrzenychPrzesuniec | usuwanie kaskadowe |
| tabelaZewnetrznychPrzesuniec_wystawy | usuwanie kaskadowe |
| wystawy_sale                          | usuwanie kaskadowe |
| sale_siedziby                         | usuwanie kaskadowe |
| wystawy_bilety_wystawy                | usuwanie kaskadowe |
| bilety_wystawy_bilety                 | usuwanie kaskadowe |
| bilety_bilety_wystawyWyjazdowe        | usuwanie kaskadowe |
| bilety_wystawywyjazdowe_WystawyWyjazdowe | usuwanie kaskadowe |