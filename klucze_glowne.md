# Klucze główne w tabelach

| Nazwa tabeli            | Nazwa klucza głównego |
|-------------------------|-----------------------|
| Siedziby                | id_Siedziby           |
| Wystawy                 | id_Wystawy            | 
| Wystawy wyjazdowe       | id_Wystawy       |
| Kategoria eksponatów    | id_Kategorii  |
| Eksponaty               | id_Eksponatu     |
| Sale                    | id_Sale              |
| Tabele lokalnych przesunięć eksponatów     | id_Przesuniecia  |
| Tabela zewnętrzych przesunięć eksponatów   | id_Przesuniecla  |
| Zakup eksponatów        | id_Zakupu        |
| Sprzedaż eksponatów     | id_Sprzedazy    |
| Konserwacje             | id_Konserwacji   |
| Konserwacje - Materiały | id         |
| Materiały               | id_Materialu     |
| Zakup materialu         | id_Zakupu        |
| Dostawcy                | id_Dostawcy      | 
| Pracownicy              | id_Pracownika    |
| Bilety                  | id_Biletu        |
| Bilety - wystawy lokalne| id_BiletWystawa  |
| Bilety - wystawy zewnętrzne | id_BiletWystawa | 