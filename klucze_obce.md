## Tabela Wystawy 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Sale                    | id_Sali             |

## tabela Kategorie Eksponatów 
| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Kategorie eksponatów    | id_KategoriiNadgrzednej|

## Tabela Eksponaty 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Kategorie Eksponatów    | id_Kategorii        |

## Tabela Sale

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Siedziby                | id_Siedziby         |

## Tabela lokalnych przesunięć eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |
| Sala                    | id_saliZ            |
| Sala                    | id_saliDo           |

## Tabela zewnętrznych przesunięć eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |
| Wystawa zewnętrzna      | id_wystawyZ         |
| Wystawa zewnętrzna      | id_wystawyDo        |

## Tabela Zakup Eksponatów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Sale                    | id_SaliDocelowej    |
| Eksponaty               | id_Eksponaty        |

## Tabela Sprzedaż Ekspoantów

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |

## Tabela Konserwacje

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Eksponaty               | id_Eksponatu        |

## Tabela Konserwacja - Materiały 

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Konserwacje             | id_Konserwacji      |
| Materiały               | id_Materiału        |

## Tabela bilety - wystawy lokalne

| Nazwa tabeli pierwotnej | Nazwa klucza obcego |
|-------------------------|---------------------|
| Bilety                  | id_Biletu           |
| Wystawy                 | id_WystawaLokalna   |

## Tabela bilety - wystawy zewnętrzne

| Nazwa tabeli pierwotnej | Nazwa klucza obcego  |
|-------------------------|----------------------|
| Bilety                  | id_Biletu            |
| Wystawy zewnętrzne      | id_WystawaZewnetrzna |