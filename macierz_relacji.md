<table>
    <thead>
        <tr>
            <th>Tabela/Tabela</th>
            <th>Siedziby</th>
            <th>Wystawy</th>
            <th>Wystawy wyjazdowe</th>
            <th>Kategorie eksponatów</th>
            <th>Eksponaty</th>
            <th>Sale</th>
            <th>Przesunięcia lokalne</th>
            <th>Przesunięcia zewnętrzne</th>
            <th>Zakup eksponatów</th>
            <th>Sprzedaż eksponatów</th>
            <th>Konserwacja eksponatów</th>
            <th>Zamówienia materiałów</th>
            <th>Dostawcy</th>
            <th>Pracownicy</th>
            <th>Bilety</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Siedziby</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Wystawy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td>n:1</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Wystawy wyjazdowe</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Kategorie eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td>n:1</td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Eksponaty</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td>n:1</td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Sale</b></td>
            <td>n:1</td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Przesunięcia lokalne</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td>n:m (?)</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Przesunięcia zewnętrzne</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td>n:m (?)</td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Zakup eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>1:1</td> <!-- Eksponaty --> 
            <td>n:1</td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Sprzedaż eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>1:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Konserwacja eksponatów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td>n:1</td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Zamówienia materiałów</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        <tr>
            <td><b>Dostawcy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Pracownicy</b></td>
            <td></td> <!-- Siedziby -->
            <td></td> <!-- Wystawy --> 
            <td></td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
        <tr>
            <td><b>Bilety</b></td>
            <td></td> <!-- Siedziby -->
            <td>n:m</td> <!-- Wystawy --> 
            <td>n:m</td> <!-- Wystawy wyjazdowe --> 
            <td></td> <!-- Kategorie eksponatów --> 
            <td></td> <!-- Eksponaty --> 
            <td></td> <!-- Sale --> 
            <td></td> <!-- Przesunięcia lokalne --> 
            <td></td> <!-- Przesunięcia zewnętrzne --> 
            <td></td> <!-- Zakup eksponatów --> 
            <td></td> <!-- Sprzedaż eksponatów --> 
            <td></td> <!-- Konserwacja eksponatów --> 
            <td></td> <!-- Zamówienia materiałów --> 
            <td></td> <!-- Dostawy --> 
            <td></td> <!-- Pracownicy --> 
            <td></td> <!-- Bilety --> 
        </tr>
    </tbody>
</table>