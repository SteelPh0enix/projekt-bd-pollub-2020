| Nazwa tabeli                 | Typ      | Opis                                       |
|------------------------------|----------|--------------------------------------------|
| Siedziby                     | Dane     | Informacje o siedzibach                    |
| Wystawy                      | Dane     | Informacje o wystawach                     |
| Wystawy wyjazdowe            | Dane     | Informacje o wystawach wyjazdowych         |
| Kategorie eksponatów         | Dane     | Informacje o kategoriach eksponatów        |
| Eksponaty                    | Dane     | Informacje o eksponatach                   |
| Sale                         | Dane     | Informacje o salach                        |
| Tabela lokalnych przesunięć eksponatów | Podzbiór | Informacje o przesunięciach między filiami |
| Tabela zewnętrznych przesunięć eksponatów | Podzbiór | Informacje o przesunięciach między zewnętrznymi wystawami |
| Zakup ekponatów              | Podzbiór | Informacje o zakupie eksponatów            |
| Sprzedaż eksponatów          | Podzbiór | Informacje o sprzedaży eksponatów          |
| Konserwacja eksponatów       | Podzbiór | Informacje o konserwacji                   |
| Konserwacja - materiały      | Łącząca  | Informacje o materiałach wykorzystywanych podczas konserwacji |
| Materiały konserwacyjne      | Dane     | Informacje o materiałach konserwacyjnych   |
| Zamówienia materiałów        | Podzbiór | Informacje o zamówionych materiałach       |
| Dostawcy                     | Dane     | Informacje o dostawcach materiałów         |
| Pracownicy                   | Dane     | Informacje o pracownikach                  |
| Bilety                       | Podzbiór | Informacji o biletach                      | 
| Bilety - wystawy lokalne     | Łącząca  | Informacje o biletach przypisanych do wystaw lokalnych | 
| Bilety - wystawy zewnętrzne  | Łącząca  | Informacje o biletach przypisanych do wystaw zewnętrznych | 
 