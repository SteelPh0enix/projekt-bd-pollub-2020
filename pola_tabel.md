## Siedziby

| Nazwa pola           | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                                       |
|----------------------|-------------|---------------|--------------------|--------------------------------------------|
| id_Siedziby **(id)** | INT         | TAK           | TAK                | Identyfikator siedziby muzeum, głowny klucz |
| nazwa                | VARCHAR(60) | TAK           | TAK                | Ciąg znaków okreslający nazwę muzeum       |
| miejscowosc          | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslający miejscowość        |
| ulica                | VARCHAR(60) | TAK           | NIE                | Ciag znaków określajacy ulicę              |
| nrLokum              | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslający nr mieszkania      |
| kodPocztowy          | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków określający kod pocztowy       |
| miastoPoczty         | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslający miasto             |
| telefonKontaktowy    | VARCHAR(11) | TAK           | TAK                | Ciąg znaków okreslający nr telefonu        |
| czyGlowna            | TINYINT     | TAK           | NIE                | Zmienna określająca czy siedziba jest siedzbą głowną  |

## Wystawy

| Nazwa pola            | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                 |
|-----------------------|--------------|---------------|--------------------|--------------------------------------|
| id_Wystawy  **(id)**  | INT          | TAK           | TAK                | Identyfikator wystawy, główny klucz  |
| nazwa                 | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslajacy nazwę        |
| dataStart             | TIMEDATE     | TAK           | NIE                | Data w której wystawa się zaczęła    |
| dataKoniec            | TIMEDATE     | NIE           | NIE                | Data w której wystawa się zakończyła |
| opis                  | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis wystawy  |
| koszty                | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca poniesione koszty | 
| id_Sali  **(obcy)**   | INT          | TAK           | NIE                | Identyfikator sali w siedzibie        |

## Wystawy wyjazdowe

| Nazwa pola           | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                      |
|----------------------|--------------|---------------|--------------------|-------------------------------------------|
| id_Wystawy  **(id)** | INT          | TAK           | TAK                | Identyfikator wystawy, główny klucz       |
| nazwa                | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslajacy nazwę             |
| dataStart            | TIMEDATE     | TAK           | NIE                | Data w której wystawa się zaczęła         |
| dataKoniec           | TIMEDATE     | NIE           | NIE                | Data w której wystawa się zakończyła      |
| opis                 | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis wystawy      |
| koszty               | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca poniesione koszty |
| miejscowosc          | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslający miejscowość       |
| ulica                | VARCHAR(60)  | TAK           | NIE                | Ciag znaków określajacy ulicę             |
| nrLokum              | VARCHAR(5)   | TAK           | NIE                | Ciąg znaków okreslający nr mieszkania     |
| kodPocztowy          | VARCHAR(6)   | TAK           | NIE                | Ciąg znaków określający kod pocztowy      |
| miastoPoczty         | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków okreslający miasto            |

## Kategorie eksponatów

| Nazwa pola                        | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------------|-------------|---------------|--------------------|----------------------------|
| id_Kategorii   **(id)**           | INT         | TAK           | TAK                | Identyfikator kategorii ekponatu, głowny klucz |
| nazwa                             | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy okreslający nazwę        |
| id_KategoriiNadrzednej **(obcy)** | INT         | NIE           | NIE                | Identyfikator kategorii nadrzędnej    |

## Eksponaty

| Nazwa pola              | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                                     |
|-------------------------|--------------|---------------|--------------------|------------------------------------------|
| id_Eksponatu  **(id)**  | INT          | TAK           | TAK                | Identyfikator eksponatu, głowny klucz    |
| nazwa                   | VARCHAR(60)  | TAK           | NIE                | Ciąg znakowy okreslający nazwę eksponatu |
| opis                    | VARCHAR(255) | TAK           | NIE                | Ciąg znaków określający opis eksponatu   |
| dataPowstania           | DATETIME     | NIE           | NIE                | Data powstania eksponatu                 |
| autor                   | VARCHAR(60)  | NIE           | NIE                | Ciąg znakowy określający autora          |
| material                | VARCHAR(60)  | NIE           | NIE                | Ciąg znakowy okreslający materiał        |
| id_Kategorii **(obcy)** | INT          | NIE           | NIE                | Identyfikator kategori do której należy eksponat |

## Sale

| Nazwa pola              | Typ        | Czy wymagane? | Wartość unikatowa? | Opis                           |
|-------------------------|------------|---------------|--------------------|--------------------------------|
| id_Sali     **(id)**    | INT        | TAK           | TAK                | Identyfikator sali, głowny klucz   |
| nazwa                   | VARCHAR(60)| TAK           | NIE                | Ciąg znaków okreslający nazwę sali | 
| id_Siedziby **(obcy)**  | INT        | TAK           | NIE                | Identyfikator siedziby w której jest sala |


## Tabela lokalnych przesunięć eksponatów

| Nazwa pola               | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                                        |
|--------------------------|----------|---------------|--------------------|---------------------------------------------|
| id_Przesuniecia **(id)** | INT      | TAK           | TAK                | Identyfikator przesunięcia eksponatu, głowny klucz     |
| dataPrzesuniecia         | TIMEDATE | TAK           | NIE                | Data przesuniecia eksponatu                            |
| id_Eksponatu **(obcy)**  | INT      | TAK           | NIE                | Identyfikator przenoszonego eksponatu                  |
| id_saliZ **(obcy)**      | INT      | NIE           | NIE                | Identyfikator sali z której eksponat jest przenoszony  |
| id_saliDo **(obcy)**     | INT      | NIE           | NIE                | Identyfikator sali do której eksponat jest przenoszony |

## Tabela zewnętrzych przesunięc eksponatów

| Nazwa pola               | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                                           |
|--------------------------|----------|---------------|--------------------|------------------------------------------------|
| id_Przesuniecia **(id)** | INT      | TAK           | TAK                | Identyfikator przesunięcia eksponatu, główny klucz        |
| dataPrzesuniecia         | TIMEDATE | TAK           | NIE                | Data przesuniecia eksponatu                               |
| id_Eksponatu **(obcy)**  | INT      | TAK           | NIE                | Identyfikator przenoszonego eksponatu                     |
| id_wystawyZ **(obcy)**   | INT      | NIE           | NIE                | Identyfikator wystawy z której eksponat jest przenoszony  |
| id_wystawyDo **(obcy)**  | INT      | NIE           | NIE                | Identyfikator wystawy do której eksponat jest przenoszony |


## Zakup eksponatów

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Zakupu **(id)**          | INT         | TAK           | TAK                | Identyfikator zakupu eksponatu, głowny klucz| 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmienno przecinkowa okreslająca cenę zakupu netto |
| data                        | DATETIME    | TAK           | NIE                | Data zakupu |
| imieSprzedajacego           | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający imię        |
| nazwiskoSprzedajacego       | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający nazwisko    |
| nazwaFirmy                  | VARCHAR(60) | NIE           | NIE                | Ciąg znaków okreslajacy nazwę firmy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miejscowość |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy ulicę       |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslajacy nr mieszkania |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków okreslajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miasto Pocztowe |
| id_SaliDocelowej **(obcy)** | INT         | NIE           | NIE                | Identyfikator sali do której trafia zakupiony eksponat |
| id_Eksponatu **(obcy)**     | INT         | TAK           | TAK                | Identyfikator zakupionego eksponatu |

## Sprzedaż eksponatów 

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Sprzedazy **(id)**       | INT         | TAK           | TAK                | Identyfikator sprzedazy eksponatu,głowny klucz | 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmienno przecinkowa okreslająca cena sprzedazy netto       | 
| data                        | DATETIME    | TAK           | NIE                | Data sprzedaży             |
| imieKupujacego              | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający imię        |
| nazwiskoKupującego          | VARCHAR(45) | TAK           | NIE                | Ciąg znaków określający nazwisko    |
| nazwaFirmy                  | VARCHAR(60) | NIE           | NIE                | Ciąg znaków okreslajacy nazwę firmy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miejscowość |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy ulicę       |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znaków okreslajacy nr mieszkania |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znaków okreslajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znaków okreslajacy miasto Pocztowe |
| id_Eksponatu **(obcy)**     | INT         | TAK           | TAK                | Identyfikator sprzedanego eksponatu |

## Konserwacje

| Nazwa pola                  | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|----------|---------------|--------------------|------------------------------|
| id_Konserwacji **(id)**     | INT      | TAK           | TAK                | Identyfikator konserwacji, klucz główny |
| dataRozpoczecia             | TIMEDATE | TAK           | NIE                | Data rozpoczęcia konserwacji |
| dataZakonczenia             | TIMEDATE | NIE           | NIE                | Data zakończenia konserwacji |
| id_Eksponatu **(obcy)**     | INT      | TAK           | NIE                | Identyfikator konserwowanego przedmiotu |

### Konserwacje - materiały

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|--------------|---------------|--------------------|------------------------------|
| id **(id)**                 | INT          | TAK           | TAK                | Identyfikator rekordu                   |
| iloscMaterialu              | DOUBLE       | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca ilość posiadanego materiału      |
| jednostkaMaterialu          | VARCHAR(45)  | TAK           | NIE                | Ciąg znakowy określajacy jednostkę materiału |
| id_Konserwacji **(obcy)**   | INT          | TAK           | NIE                | Identyfikator konserwacji               |
| id_Materialu **(obcy)**     | INT          | TAK           | NIE                | Identyfikator uzytego materialu         |

## Materialy konserwacyjne

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                         |
|-----------------------------|--------------|---------------|--------------------|------------------------------|
| id_Materialu **(id)**       | INT          | TAK           | TAK                | Identyfikator materiału, klucz głowny   |
| nazwa                       | VARCHAR(60)  | TAK           | NIE                | Ciąg znaków określający nazwę materiału |
| opisMaterialu               | VARCHAR(255) | TAK           | NIE                | Ciąg znaków okreslajacy opis materiału  |
| iloscMaterialu              | DOUBLE       | TAK           | NIE                | Liczba zmiennoprzecinkowa okrelająca ilośc materiału |
| jednostkaMaterialu          | VARCHAR(60)  | TAK           | NIE                | Ciąg znakowy określajacy jednostkę materiału |

## Zamówienia materiałów
  
| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Zakupu **(id)**          | INT         | TAK           | TAK                | Identyfikator zakupu materiału, klucz głowny       | 
| cena                        | DECIMAL     | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca cenę netto   |
| data                        | DATETIME    | TAK           | NIE                | Data zakupu |
| ilosc                       | DOUBLE      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca ilosc zakupionego materialu |
| jednostka                   | VARCHAR(60) | TAK           | NIE                | Jednostka zakupionego materiału                    |
| nazwiskoSprzedajacego       | VARCHAR(45) | TAK           | NIE                | Ciąg znakowy określajacy nazwisko                  |
| imieSprzedajacego           | VARCHAR(45) | TAK           | NIE                | Ciąg znakowy określajacy imie                      |
| firmaSprzedajacego          | VARCHAR(60) | NIE           | NIE                | Ciąg znakowy określajacy nazwa firmy               |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość               |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulica                     |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu              |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy              |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej |

## Dostawcy

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Dostawcy **(id)**        | INT         | TAK           | TAK                | Identyfikator dostawcy, klucz głowny    |
| nazwaDostawcy               | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy nazwa dostawcy |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość    |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulicę          |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu   |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy   |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej  |

## Pracownicy 

| Nazwa pola                  | Typ         | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|-------------|---------------|--------------------|----------------------------|
| id_Pracownika **(id)**      | INT         | TAK           | TAK                | Identyfikator pracownika, klucz głowny | 
| imie                        | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy imię          |
| nazwisko                    | VARCHAR     | TAK           | NIE                | Ciąg znakowy określajacy nazwisko      |
| pesel                       | VARCHAR(11) | TAK           | NIE                | Ciąg znakowy określajacy pesel         |
| miejscowosc                 | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miejscowość   |
| ulica                       | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy ulicę         |
| nrLokum                     | VARCHAR(5)  | TAK           | NIE                | Ciąg znakowy określajacy numer lokalu  |
| kodPocztowy                 | VARCHAR(6)  | TAK           | NIE                | Ciąg znakowy określajacy kod pocztowy  |
| miastoPoczty                | VARCHAR(60) | TAK           | NIE                | Ciąg znakowy określajacy miasto siedziby pocztowej | 
| numerTelefonu               | VARCHAR(11) | TAK           | NIE                | Ciąg znakowy określajacy numer telefonu |

## Bilety

| Nazwa pola                  | Typ          | Czy wymagane? | Wartość unikatowa? | Opis                       |
|-----------------------------|--------------|---------------|--------------------|----------------------------|
| id_Biletu **(id)**          | INT          | TAK           | TAK                | Identyfikator biletu, klucz główny                |
| cena                        | DECIMAL      | TAK           | NIE                | Liczba zmiennoprzecinkowa okreslająca cenę biletu |
| opis                        | VARCHAR(255) | TAK           | NIE                | Ciąg znakowy określajacy opis biletu              |

## Bilety - wystawy lokalne

| Nazwa pola                   | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                       |
|------------------------------|----------|---------------|--------------------|----------------------------|
| id_BiletWystawa **(id)**     | INT      | TAK           | TAK                | Identyfikator połączenia bilet-wystawa, klucz główny |
| id_Biletu **(obcy)**         | INT      | TAK           | NIE                | Identyfikator biletu                   |
| id_WystawaLokalna **(obcy)** | INT      | TAK           | NIE                | Identyfikator wystawy lokalnej         |

## Bilety - wystawy zewnętrzne

| Nazwa pola                      | Typ      | Czy wymagane? | Wartość unikatowa? | Opis                       |
|---------------------------------|----------|---------------|--------------------|----------------------------|
| id_BiletWystawa **(id)**        | INT      | TAK           | TAK                | Identyfikator połączenia bilet-wystawa, klucz główny |
| id_Biletu **(obcy)**            | INT      | TAK           | NIE                | Identyfikator biletu                   |
| id_WystawaZewnetrzna **(obcy)** | INT      | TAK           | NIE                | Identyfikator wystawy zewnętrznej      |