| Rodzaj związku                     | Reguła usuwania danych z bazy        |
|------------------------------------|--------------------------------------|
| sale_zakupEksponatow               | Ustawienie wartości 'null' dla atrybutu w tabeli Zakup Eksponatów |
| eksponaty_zakupeksponatów          | usuwanie kaskadowe   |
| eksponaty_kategorieEksponatów      | Ustawienie wartości 'null' dla atrybutu w tabeli Eksponaty |
| eksponaty_konserwacje              | usuwanie kaskadowe                   |
| konserwacje_konserwacje_MateriałyKonserwacyjne | usuwanie kaskadowe |
| konserwacje-MaterialyKonserwacyjne_MaterialyKonseracyjne | usuwanie kaskadowe |
| sprzedaż_Eksponatów_Eksponaty      | usuwanie kaskadowe               |
| eksponaty_tabelaLokalnychPrzesuniec | usuwanie kaskadowe |
| eksponaty_tabelaZewnetrzenychPrzesuniec | usuwanie kaskadowe |
| tabelaZewnetrznychPrzesuniec_wystawy | usuwanie kaskadowe |
| wystawy_sale                          | usuwanie kaskadowe |
| sale_siedziby                         | usuwanie kaskadowe |
| wystawy_bilety_wystawy                | usuwanie kaskadowe |
| bilety_wystawy_bilety                 | usuwanie kaskadowe |
| bilety_bilety_wystawyWyjazdowe        | usuwanie kaskadowe |
| bilety_wystawywyjazdowe_WystawyWyjazdowe | usuwanie kaskadowe |